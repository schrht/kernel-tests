# storage/nvdimm/nvdimm_error_injecting_test

Storage: nvdimm inject error testing for raw/fsdax/devdax/btt

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
